#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import shutil
import re
import os


def main(move_from: str, move_to: str, regEx: str=".*-unsplash.\w+$"):
    _from, _to = os.path.abspath(move_from), os.path.abspath(move_to)
    _from_content = os.listdir(_from)
    for _path in _from_content:
        if re.search(regEx, _path) and not os.path.isfile("{}{}{}".format(_to, os.path.sep, _path)):
            shutil.copy("{}{}{}".format(_from, os.path.sep, _path), _to)


if __name__ == "__main__":
    if os.path.isdir(sys.argv[1]) and os.path.isdir(sys.argv[2]):
        if len(sys.argv) == 4:
            main(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            main(sys.argv[1], sys.argv[2])
    else:
        print("Path not found")
